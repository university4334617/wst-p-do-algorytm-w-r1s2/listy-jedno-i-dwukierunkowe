import random
import numpy as np
import matplotlib.pyplot as plt

#Zadanie 1
class Wezel_listy_1kier: #Popek
    def __init__(self, dane_wezla=None, next=None):
        self.dane_wezla = dane_wezla
        self.next = next

    def __str__(self): #Zmieniamy węzeł na str
        return str(self.dane_wezla)

    def copy(self): #Kopiujemy węzeł i rekurencyjne resztę listy
        if self is None:
            return None
        else:
            new_node = Wezel_listy_1kier(self.dane_wezla)
            new_node.next = self.next.copy() if self.next else None
            return new_node

def losuj_zlozonosc(okienko): #Zadanie 1b
    if okienko == 'A':
        return random.randint(1, 4)
    elif okienko == 'B':
        return random.randint(5, 8)
    elif okienko == 'C':
        return random.randint(9, 12)
    elif okienko == 'E':
        return random.randint(1, 12)

def losuj_kolejke(n):
    pierwszy = None
    for i in range(n):
        typ_zadania = random.choice(['A', 'B', 'C'])
        pierwszy = Wezel_listy_1kier([typ_zadania, losuj_zlozonosc(typ_zadania)], pierwszy)
    return pierwszy

def czas_w_okienkach(urzad):
    for okienko in urzad:
        okienko['czas'] = losuj_zlozonosc(okienko['okienko'])  # losowy czas oczekiwania

def traverse(node, okienko): #Popek #Patrzymy dla poprzednika
    while node:
        if node.next and node.next.dane_wezla[0] == okienko:
            return node
        node = node.next

def print_forward(node): #Popek #Rekurencyjne wypisanie listy jednokierunkowej 
    if node:
        print(node)
        print_forward(node.next)

def symulation(urzad, pierwszy): #Zadanie 1c
    czas = 0
    aktualny_klient = pierwszy
    while aktualny_klient or any(okienko['czas'] > 0 for okienko in urzad):
        czas += 1
        for okienko in urzad:
            if okienko['czas'] > 0:
                okienko['czas'] -= 1
                if okienko['czas'] == 0: #Jak jest 0 to znaczy, że właśnie kogoś obsłużono
                    okienko['licznik_k'] += 1

            elif okienko['okienko'] == 'E':
                if aktualny_klient:
                    typ_zadania, zlozonosc = aktualny_klient.dane_wezla
                    okienko['czas'] = zlozonosc
                    aktualny_klient = aktualny_klient.next
                    if not aktualny_klient:
                        break

            elif aktualny_klient:
                inny_klient = traverse(aktualny_klient, okienko['okienko'])
                if aktualny_klient.dane_wezla[0] == okienko['okienko']: #Jeśli jest wolne okno dla pierwszego
                    typ_zadania, zlozonosc = aktualny_klient.dane_wezla
                    okienko['czas'] = zlozonosc
                    aktualny_klient = aktualny_klient.next
                    if not aktualny_klient:
                        break

                elif inny_klient: #Jeśli okienko dla pierwszego jest zajęte
                    if inny_klient.next.dane_wezla[0] == okienko['okienko']:
                        typ_zadania, zlozonosc = inny_klient.next.dane_wezla
                        okienko['czas'] = zlozonosc
                        inny_klient.next = inny_klient.next.next
    return czas, urzad

okienka_1 = [
    {'okienko': 'A', 'czas': 0, 'licznik_k': 0}, {'okienko': 'A', 'czas': 0, 'licznik_k': 0}, {'okienko': 'A', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'E', 'czas': 0, 'licznik_k': 0}
]

czas_w_okienkach(okienka_1)
pierwszy = losuj_kolejke(40)
print("Zadanie 1d - stworzyć urząd:\n", symulation(okienka_1, pierwszy)) 

###################################################################################################################################################################################
#Zadanie 2

def kopiuj_urzad(urzad):
    kopia = []
    for okienko in urzad:
        kopia.append(okienko.copy())
    return kopia


def bubblesort_malejaco(node):
    head1 = node
    if not head1 or not head1.next:
        return head1
    
    swapped = True #Ustawienie flagi określającej czy miała miejsce zamiana w bieżącej iteracji

    while swapped:
        swapped = False
        prev = None  #Poprzedni węzeł
        current = head1  #Bieżący węzeł
        next_node = current.next  #Następny węzeł

        while next_node:
            if current.dane_wezla[1] < next_node.dane_wezla[1]: #Sprawdzenie, czy wartość w bieżącym węźle jest mniejsza od wartości w następnym węźle
                current.dane_wezla, next_node.dane_wezla = next_node.dane_wezla, current.dane_wezla #Zamiana wartości danych między bieżącym a następnym węzłem
                swapped = True
            prev = current
            current = next_node
            next_node = next_node.next

    return head1


def bubblesort_rosnaco(node):
    if node is None or node.next is None:
        return node

    swapped = True
    while swapped:
        swapped = False
        prev = None
        current = node
        next_node = node.next

        while next_node:
            if current.dane_wezla[1] > next_node.dane_wezla[1]:
                if prev:
                    prev.next = next_node
                else:
                    node = next_node
                current.next, next_node.next = next_node.next, current
                current, next_node = next_node, current
                swapped = True

            prev, current, next_node = current, current.next, next_node.next

    return node


def testuj_kolejke(urzad, kolejka):
    czas, _ = symulation(kopiuj_urzad(urzad), kolejka.copy())
    return czas


def losuj_i_testuj(liczba_kolejek, okienka_2_1, okienka_2_2, okienka_2_3):
    t_u1 = []
    t_u2 = []
    t_u3 = []
    for _ in range(liczba_kolejek):
        kolejka = losuj_kolejke(random.randint(20, 50))
        czas1 = testuj_kolejke(kopiuj_urzad(okienka_2_1), kolejka.copy())
        czas2 = testuj_kolejke(kopiuj_urzad(okienka_2_2), kolejka.copy())
        czas3 = testuj_kolejke(kopiuj_urzad(okienka_2_3), kolejka.copy())
        t_u1.append(czas1)
        t_u2.append(czas2)
        t_u3.append(czas3)
    return t_u1, t_u2, t_u3


def losuj_i_testuj_uporzadkowane(liczba_kolejek, okienka_2_1, okienka_2_2, okienka_2_3):
    t_u1_rosnaco = []
    T_u2_rosnaco = []
    t_u3_rosnaco = []

    t_u1_malejaco = []
    T_u2_malejaco = []
    t_u3_malejaco = []

    for _ in range(liczba_kolejek):
        kolejka = losuj_kolejke(random.randint(20, 50))
        kolejka_rosnaco = bubblesort_rosnaco(kolejka.copy())
        kolejka_malejaco = bubblesort_malejaco(kolejka.copy())

        czas1_rosnaco = testuj_kolejke(kopiuj_urzad(okienka_2_1), kolejka_rosnaco.copy())
        czas2_rosnaco = testuj_kolejke(kopiuj_urzad(okienka_2_2), kolejka_rosnaco.copy())
        czas3_rosnaco = testuj_kolejke(kopiuj_urzad(okienka_2_3), kolejka_rosnaco.copy())

        czas1_malejaco = testuj_kolejke(kopiuj_urzad(okienka_2_1), kolejka_malejaco.copy())
        czas2_malejaco = testuj_kolejke(kopiuj_urzad(okienka_2_2), kolejka_malejaco.copy())
        czas3_malejaco = testuj_kolejke(kopiuj_urzad(okienka_2_3), kolejka_malejaco.copy())

        t_u1_rosnaco.append(czas1_rosnaco)
        T_u2_rosnaco.append(czas2_rosnaco)
        t_u3_rosnaco.append(czas3_rosnaco)

        t_u1_malejaco.append(czas1_malejaco)
        T_u2_malejaco.append(czas2_malejaco)
        t_u3_malejaco.append(czas3_malejaco)

    return (t_u1_rosnaco, T_u2_rosnaco, t_u3_rosnaco,
            t_u1_malejaco, T_u2_malejaco, t_u3_malejaco)


def rysuj_histogramy(t_u1, t_u2, t_u3):
    bins = np.linspace(0, max(max(t_u1), max(t_u2), max(t_u3)), 20)
    plt.hist(t_u1, bins=bins, alpha=0.5, label='Urzad 1')
    plt.hist(t_u2, bins=bins, alpha=0.5, label='Urzad 2')
    plt.hist(t_u3, bins=bins, alpha=0.5, label='Urzad 3')
    plt.xlabel('Czas obsługi kolejki')
    plt.ylabel('Liczba kolejek')
    plt.title(f'Histogramy czasów obsługi kolejek w różnych wersjach urzędu')
    plt.legend()
    plt.show()


def rysuj_histogramy_uporzadkowane(t_u1_rosnaco, T_u2_rosnaco, t_u3_rosnaco, t_u1_malejaco, T_u2_malejaco, t_u3_malejaco):
    min_val_rosnaco = min(min(t_u1_rosnaco), min(T_u2_rosnaco), min(t_u3_rosnaco))
    max_val_rosnaco = max(max(t_u1_rosnaco), max(T_u2_rosnaco), max(t_u3_rosnaco))

    min_val_malejaco = min(min(t_u1_malejaco), min(T_u2_malejaco), min(t_u3_malejaco))
    max_val_malejaco = max(max(t_u1_malejaco), max(T_u2_malejaco), max(t_u3_malejaco))

    bins_rosnaco = np.linspace(min_val_rosnaco, max_val_rosnaco, 20)
    bins_malejaco = np.linspace(min_val_malejaco, max_val_malejaco, 20)

    plt.subplot(1, 2, 1)
    plt.hist(t_u1_rosnaco, bins=bins_rosnaco, alpha=0.5, label='Urzad 1 - Rosnąco')
    plt.hist(T_u2_rosnaco, bins=bins_rosnaco, alpha=0.5, label='Urzad 2 - Rosnąco')
    plt.hist(t_u3_rosnaco, bins=bins_rosnaco, alpha=0.5, label='Urzad 3 - Rosnąco')
    plt.xlabel('Czas obsługi kolejki')
    plt.ylabel('Liczba kolejek')
    plt.title('Czas obsługi kolejek w urzędach (rosnąco)')
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.hist(t_u1_malejaco, bins=bins_malejaco, alpha=0.5, label='Urzad 1 - Malejąco')
    plt.hist(T_u2_malejaco, bins=bins_malejaco, alpha=0.5, label='Urzad 2 - Malejąco')
    plt.hist(t_u3_malejaco, bins=bins_malejaco, alpha=0.5, label='Urzad 3 - Malejąco')
    plt.xlabel('Czas obsługi kolejki')
    plt.ylabel('Liczba kolejek')
    plt.title('Czas obsługi kolejek w urzędach (malejąco)')
    plt.legend()

    plt.tight_layout()
    plt.show()


#Zadanie 2a
okienka_2_1 = [
    {'okienko': 'A', 'czas': 0, 'licznik_k': 0}, {'okienko': 'A', 'czas': 0, 'licznik_k': 0}, {'okienko': 'A', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0}
]
okienka_2_2 = [
    {'okienko': 'A', 'czas': 0, 'licznik_k': 0}, {'okienko': 'A', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'E', 'czas': 0, 'licznik_k': 0}, {'okienko': 'E', 'czas': 0, 'licznik_k': 0}, {'okienko': 'E', 'czas': 0, 'licznik_k': 0}
]
okienka_2_3 = [
    {'okienko': 'A', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'B', 'czas': 0, 'licznik_k': 0}, {'okienko': 'B', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0}, {'okienko': 'C', 'czas': 0, 'licznik_k': 0},
    {'okienko': 'E', 'czas': 0, 'licznik_k': 0}
]

#Zadanie 2b
kolejka = losuj_kolejke(30)
print('Urząd 1:', testuj_kolejke(okienka_2_1, kolejka),
        '\nUrząd 2:', testuj_kolejke(okienka_2_2, kolejka),
        '\nUrząd 3:', testuj_kolejke(okienka_2_3, kolejka))


#Zadanie 2c
t_u1, t_u2, t_u3 = losuj_i_testuj(100, okienka_2_1, okienka_2_2, okienka_2_3)

sredni_t1 = np.mean(t_u1)
sredni_t2 = np.mean(t_u2)
sredni_t3 = np.mean(t_u3)
print('Urząd 1:', sredni_t1,
        '\nUrząd 2:', sredni_t2,
        '\nUrząd 3:', sredni_t3)

rysuj_histogramy(t_u1, t_u2, t_u3)

t_u1_rosnaco, t_u2_rosnaco, t_u3_rosnaco, t_u1_malejaco, t_u2_malejaco, t_u3_malejaco = losuj_i_testuj_uporzadkowane(100, okienka_2_1, okienka_2_2, okienka_2_3)
rysuj_histogramy_uporzadkowane(t_u1_rosnaco, t_u2_rosnaco, t_u3_rosnaco, t_u1_malejaco, t_u2_malejaco, t_u3_malejaco)
    